# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

    Just Open the Page and start to use it, the canvas is in the middle 
    of the page and the tools are in both sides of the page.
    

### Function description
    Function of My Canvas
    
    TOOL
        Pencil
        1.available to choose 5 kinds pen size
        2.available to change color
    
        Brush
        1.available to choose 5 kinds brush size
        2.available to change color
    
        Eraser:
        1.available to wipe things out
    
        Bucket:
        1.available to fill in colors with borders
    
    Shapes:
        Line:draw a line
        Triangle:draw a triangle
        Circle:draw a circle
        Rectangle:draw a rectangle
    
    Commands:
        Undo:available to get a step before
        Redo:not available yet
        Text:not available yet
        Download:available to get what you have drawn
        Upload:not available yet
        Clear:available to restart


### Gitlab page link

    https://106030028.gitlab.io/AS01_WebCanvas 

### Others (Optional)

    TA們辛苦了

<style>
table th{
    width: 100%;
}
</style>