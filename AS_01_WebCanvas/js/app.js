import Paint from './paint.class.js';
import Tool from './tool.class.js';

let paint  = new Paint("canvas");

// Set defaults
paint.activeTool = Tool.TOOL_LINE;
paint.lineWidth = '1';
paint.brushSize = '4';
paint.selectedColor = '#000000';

// initialize paint
paint.init();


/////

var colorBlock = document.getElementById("color-block");
var ctx1 = colorBlock.getContext("2d");
var width1 = colorBlock.width;
var height1 = colorBlock.height;

var colorStrip = document.getElementById('color-strip');
var ctx2 = colorStrip.getContext('2d');
var width2 = colorStrip.width;
var height2 = colorStrip.height;

var colorLabel = document.getElementById('color-label');

var x = 0;
var y = 0;
var drag = false;
var rgbaColor = 'rgba(255,0,0,1)';

var colorData ;


ctx1.rect(0, 0, width1, height1);
fillGradient();

ctx2.rect(0, 0, width2, height2);
var grd1 = ctx2.createLinearGradient(0, 0, 0, height1);
grd1.addColorStop(0, 'rgba(255, 0, 0, 1)');
grd1.addColorStop(0.17, 'rgba(255, 255, 0, 1)');
grd1.addColorStop(0.34, 'rgba(0, 255, 0, 1)');
grd1.addColorStop(0.51, 'rgba(0, 255, 255, 1)');
grd1.addColorStop(0.68, 'rgba(0, 0, 255, 1)');
grd1.addColorStop(0.85, 'rgba(255, 0, 255, 1)');
grd1.addColorStop(1, 'rgba(255, 0, 0, 1)');
ctx2.fillStyle = grd1;
ctx2.fill();

function click(e) {
  x = e.offsetX;
  y = e.offsetY;
  var imageData = ctx2.getImageData(x, y, 1, 1).data;
  rgbaColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
  fillGradient();


}

function fillGradient() {
  ctx1.fillStyle = rgbaColor;
  ctx1.fillRect(0, 0, width1, height1);

  var grdWhite = ctx2.createLinearGradient(0, 0, width1, 0);
  grdWhite.addColorStop(0, 'rgba(255,255,255,1)');
  grdWhite.addColorStop(1, 'rgba(255,255,255,0)');
  ctx1.fillStyle = grdWhite;
  ctx1.fillRect(0, 0, width1, height1);

  var grdBlack = ctx2.createLinearGradient(0, 0, 0, height1);
  grdBlack.addColorStop(0, 'rgba(0,0,0,0)');
  grdBlack.addColorStop(1, 'rgba(0,0,0,1)');
  ctx1.fillStyle = grdBlack;
  ctx1.fillRect(0, 0, width1, height1);
}

function mousedown(e) {
  drag = true;
  changeColor(e);
}

function mousemove(e) {
  if (drag) {
    changeColor(e);
  }
}

function mouseup(e) {
  drag = false;
}

function changeColor(e) {
  x = e.offsetX;
  y = e.offsetY;
  var imageData = ctx1.getImageData(x, y, 1, 1).data;
  rgbaColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
  colorLabel.style.backgroundColor = rgbaColor;

}


colorStrip.addEventListener("click", click, false);

colorBlock.addEventListener("mousedown", mousedown, false);
colorBlock.addEventListener("mouseup", mouseup, false);
colorBlock.addEventListener("mousemove", mousemove, false);

/////
document.querySelectorAll("[data-command]").forEach(
    (el) => {
        el.addEventListener("click", (e) => {
            let command = el.getAttribute('data-command');
            
            if(command == 'undo' ){
                paint.undoPaint();
            }else if(command == 'redo'){
                paint.redoPaint();
            }
            else if(command == 'download'){
                var canvas = document.getElementById("canvas");
                var image = canvas.toDataURL("image/png", 1.0).replace("image/png", "image/octet-stream");
                var link = document.createElement('a');
                link.download = "my-image.png";
                link.href = image;
                link.click();
            }
        });
    }
);

document.querySelectorAll("[data-tool]").forEach(
    (el) => {
        el.addEventListener("click", (e) => {
            document.querySelector("[data-tool].active").classList.toggle("active");
            el.classList.toggle("active");
            let selectedTool = el.getAttribute("data-tool");
            paint.activeTool = selectedTool;

            switch(selectedTool){
                case Tool.TOOL_LINE:
                case Tool.TOOL_RECTANGLE:
                case Tool.TOOL_CIRCLE:
                case Tool.TOOL_TRIANGLE:
                case Tool.TOOL_PENCIL:
                    document.querySelector(".group.pencil").style.display = "block";
                    document.querySelector(".group.brush").style.display = "none";
                    break;
                case Tool.TOOL_BRUSH:
                case Tool.TOOL_ERASER:
                    document.querySelector(".group.pencil").style.display = "none";
                    document.querySelector(".group.brush").style.display = "block";
                    break;
                default:
                    document.querySelector(".group.pencil").style.display = "none";
                    document.querySelector(".group.brush").style.display = "none";
            }

        });
    }
);

document.querySelectorAll("[data-line-width]").forEach(
    (el) => {
        el.addEventListener("click", (e) =>{
            document.querySelector("[data-line-width].active").classList.toggle("active");
            el.classList.toggle("active");

            paint.lineWidth = el.getAttribute("data-line-width");
        });
    }
);

document.querySelectorAll("[data-brush-size]").forEach(
    (el) => {
        el.addEventListener("click", (e) =>{
            document.querySelector("[data-brush-size].active").classList.toggle("active");
            el.classList.toggle("active");

            paint.brushSize = el.getAttribute("data-brush-size");
        });
    }
);

document.querySelectorAll("[data-color]").forEach(
    (el) => {
        el.addEventListener("click", (e) =>{
            document.querySelector("[data-color].active").classList.toggle("active");
            el.classList.toggle("active");

            paint.selectedColor = el.getAttribute("data-color");
            paint.selectedColor = rgbaColor;
        });
    }
);
